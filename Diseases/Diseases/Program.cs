﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diseases
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please insert CSV file Path");
            string userInput = Console.ReadLine();
            var dic = ReadFileReturnDic(userInput);
            while (dic == null)
            {
                Console.WriteLine("This file can not be found, Please try again");
                userInput = Console.ReadLine();
                dic = ReadFileReturnDic(userInput);
            }

            var dicToList = dic.ToList();

            Console.WriteLine("\nMost popular diseases are:");
            var topDiseases = TopDiseases(dicToList, 3);
            Console.WriteLine(string.Join(",", topDiseases.Select(a => a.Key)));

            List<List<string>> values = dic.Values.ToList();
            int count = UniqueSymptoms(values).Count;
            Console.WriteLine($"\nCount of the unique symptoms is: {count}");

            Console.WriteLine("\nMost popular symptoms are:");
            var symp = RankedSymptoms(values, 3, SortTopSymptoms);
            Console.WriteLine(string.Join(",", symp));

            Console.WriteLine("\nPlease enter the symptoms divided by ','");
            userInput = Console.ReadLine();
            List<string> symptoms = userInput.Split(',').ToList();
            PossibleDiseases(symptoms, dicToList);

            GuessDisease(dic);

            Console.WriteLine("\nPress enter to exit");
            Console.ReadLine();
        }

        //c:\users\ekuldkep\Documents\Csv\Sickness10.csv
        public static Dictionary<string, List<string>> ReadFileReturnDic(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
            {
                return null;
            }

            StreamReader reader = new StreamReader(File.OpenRead(filePath));

            var dic = new Dictionary<string, List<string>>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');

                if (!String.IsNullOrEmpty(values[0]))
                {
                    string key = values[0];
                    List<String> symptoms = values.ToList();
                    symptoms.Remove(key);
                    dic[key] = symptoms;
                }
            }
            return dic;
        }

        public static List<KeyValuePair<string, List<string>>> TopDiseases(List<KeyValuePair<string, List<string>>> list, int count)
        {
            var topDiseases = list.OrderByDescending(a => a.Value.Distinct().Count()).ThenBy(a => a.Key).Take(count).ToList();

            return topDiseases;
        }

        public static List<string> UniqueSymptoms(List<List<string>> values)
        {
            List<string> allSymptoms = new List<string>();

            foreach (List<string> val in values)
            {
                allSymptoms = allSymptoms.Concat(val).ToList();
            }

            return allSymptoms.Distinct().ToList();
        }

        public static List<IGrouping<string, string>> SortMedianSymptoms(List<IGrouping<string, string>> groups, int count)
        {
            return groups.OrderBy(grp => Math.Abs(grp.Count() - count / 2)).ToList();
        }

        public static List<IGrouping<string, string>> SortTopSymptoms(List<IGrouping<string, string>> groups, int count)
        {
            return groups.OrderByDescending(grp => grp.Count()).ThenBy(gpr => gpr.Key).ToList();
        }

        public static List<string> RankedSymptoms(List<List<string>> values, int nr, Func<List<IGrouping<string, string>>, int, List<IGrouping<string, string>>> sortFunc)
        {
            List<string> allSymptoms = new List<string>();

            foreach (List<string> symptoms in values)
            {
                allSymptoms = allSymptoms.Concat(symptoms.Distinct()).ToList();
            }

            var groups = allSymptoms.GroupBy(i => i).ToList();
            var sorted = sortFunc(groups, values.Count);
            var result = sorted.Select(grp => grp.Key)
                    .Take(nr)
                    .ToList();
            return result;
        }

        public static bool ContainsAll<T>(List<T> source, List<T> values)
        {
            return values.All(source.Contains);
        }

        public static void PossibleDiseases(List<string> symptoms, List<KeyValuePair<string, List<string>>> list)
        {
            bool hasMatches = false;
            foreach (var dis in list)
            {
                if (ContainsAll(dis.Value, symptoms))
                {
                    hasMatches = true;
                    Console.WriteLine($"You might have {dis.Key}");
                }
            }
            if (!hasMatches)
            {
                Console.WriteLine("I do not know such disease");
            }
        }

        public static void GuessDisease(Dictionary<string, List<string>> dic)
        {
            while (dic.Count > 0)
            {
                var medSymp = RankedSymptoms(dic.Values.ToList(), 1, SortMedianSymptoms)[0];
                Console.WriteLine($"Do you have {medSymp}? [yes/no]");

                var input = Console.ReadLine();
                var yes = input.Equals("yes", StringComparison.OrdinalIgnoreCase);
                foreach (var keyValuePair in dic.ToList())
                {
                    if (yes)
                    {
                        if (!keyValuePair.Value.Contains(medSymp))
                        {
                            dic.Remove(keyValuePair.Key);
                        }
                        else
                        {
                            keyValuePair.Value.Remove(medSymp);
                        }
                    }
                    else
                    {
                        if (keyValuePair.Value.Contains(medSymp))
                        {
                            dic.Remove(keyValuePair.Key);
                        }
                        else
                        {
                            keyValuePair.Value.Remove(medSymp);
                        }
                    }
                }

                if (dic.Count.Equals(1))
                {
                    Console.WriteLine($"You have {dic.First().Key}");
                    break;
                }
            }
        }
    }
}
